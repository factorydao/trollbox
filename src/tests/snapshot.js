import { mount } from 'enzyme';
import enzymToJson from 'enzyme-to-json';
import { act as testRendererAct, create } from 'react-test-renderer';

export default function snapshot(target, { useEnzyme } = {}) {
  let component;

  let tree;

  if (useEnzyme) {
    component = mount(target, typeof useEnzyme === 'object' && useEnzyme);
    tree = enzymToJson(component);
  } else {
    testRendererAct(() => {
      component = create(target);
    });

    tree = component.toJSON();
  }

  expect(tree).toMatchSnapshot();
}
