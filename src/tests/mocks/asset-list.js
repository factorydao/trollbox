const Comp = 'assets/icons/comp.png';
const Dai = 'assets/icons/dai.png';
const Link = 'assets/icons/link.png';
const Mkr = 'assets/icons/mkr.png';
const Ren = 'assets/icons/ren.png';
const Snx = 'assets/icons/snx.png';
const Uma = 'assets/icons/uma.png';
const Uni = 'assets/icons/uni.png';
const Wbtc = 'assets/icons/wbtc.png';
const Yfi = 'assets/icons/yfi.png';

const Ada = 'assets/icons/ada.png';
const Bch = 'assets/icons/bch.png';
const Bnb = 'assets/icons/bnb.png';
const Btc = 'assets/icons/btc.png';
const Busd = 'assets/icons/busd.png';
const Dot = 'assets/icons/dot.png';
const Eth = 'assets/icons/eth.png';
const Fil = 'assets/icons/fil.png';
const Ltc = 'assets/icons/ltc.png';
const Matic = 'assets/icons/matic.png';
const Sand = 'assets/icons/sand.png';
const Trx = 'assets/icons/trx.png';
const Xrp = 'assets/icons/xrp.png';

const logos = {
  sand: Sand,
  btc: Btc,
  matic: Matic,
  comp: Comp,
  dai: Dai,
  link: Link,
  snx: Snx,
  yfi: Yfi,
  wbtc: Wbtc,
  mkr: Mkr,
  uma: Uma,
  uni: Uni,
  ren: Ren,
  ada: Ada,
  bch: Bch,
  bnb: Bnb,
  busd: Busd,
  dot: Dot,
  eth: Eth,
  fil: Fil,
  ltc: Ltc,
  trx: Trx,
  xrp: Xrp
};
//  {
//    icon: Ant,
//    name: 'Aragon',
//    symbol: 'ANT',
//    price: {
//      symbol: '$',
//      value: 6.96,
//      difference: 15.32
//    }
//  }

export default logos;
