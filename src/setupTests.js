// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom';
import 'tests/enzyme';
import 'tests/mocks';
import encoding from 'text-encoding';

global.TextDecoder = encoding.TextDecoder;
global.TextEncoder = encoding.TextEncoder;
