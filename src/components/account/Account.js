import classnames from 'classnames';
import AccountTransfer from 'components/account/transfer';
import Loader from 'components/containers/loader';
import IdentityProfile from 'components/identity/profile';
import PropTypes from 'prop-types';
import { memo } from 'react';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import './Account.scss';
import useAccount from './useAccount';

function Account({ className, ...props }) {
  const {
    hasIdentitiesForAccount,
    identitiesForAccount,
    identitiesForAccountError,
    identitiesForAccountAreLoading,
    showTransferDialog,
    handleCloseTransferDialog
    // handleShowTransferDialog
  } = useAccount();

  return (
    <div {...props} className={classnames('account', className)}>
      <Loader error={identitiesForAccountError} isLoading={identitiesForAccountAreLoading}>
        <Container fluid>
          <Row className="account__menu">
            <Col className="account__title" md={3} xs={12}>
              My Account
            </Col>
            {/* <Col className="account__claim-power text-end" md={9} xs={12}>
              {hasIdentitiesForAccount && (
                <Button variant="danger" onClick={handleShowTransferDialog}>
                  Transfer
                </Button>
              )}
            </Col> */}
          </Row>
          <Row>
            <Col>
              {hasIdentitiesForAccount ? (
                <div className="account__identities">
                  {Object.values(identitiesForAccount).map(({ tokenId, unclaimedTokens }) => (
                    <IdentityProfile
                      key={tokenId}
                      tokenId={tokenId}
                      unclaimedFVT={unclaimedTokens}
                    />
                  ))}
                </div>
              ) : (
                <p>No accounts detected for this address</p>
              )}
            </Col>
          </Row>
        </Container>
      </Loader>
      <AccountTransfer
        identities={identitiesForAccount || {}}
        show={showTransferDialog}
        onClose={handleCloseTransferDialog}
      />
    </div>
  );
}

Account.propTypes = {
  className: PropTypes.string
};

export default memo(Account);
