import { toChecksumAddress } from 'evm-chain-scripts';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { transferVoterID } from 'utils/simpleSetters';

export default function useAccountTransfer({ identities } = {}) {
  const identitySelectableOptions = useMemo(
    () =>
      Object.values(identities).map((identity) => ({
        value: identity.tokenId,
        label: `FVT ${identity.tokenId}`
      })),
    [identities]
  );

  const [selectedIdentity, setSelectedIdentity] = useState(null);

  const defaultSelectableIdentity = useMemo(
    () =>
      selectedIdentity &&
      identitySelectableOptions?.find(({ value }) => value === selectedIdentity),
    [identitySelectableOptions, selectedIdentity]
  );

  const [address, setAddress] = useState('');

  const [addressCheckSumCorrect, setAddressCheckSumCorrect] = useState(false);

  const transfer = useCallback(() => {
    transferVoterID(selectedIdentity, address);
  }, [selectedIdentity, address]);

  const handleIdentityChange = useCallback(
    ({ value }) => {
      setSelectedIdentity(identities[value]);
    },
    [identities]
  );

  const handleAddressChange = useCallback(({ target: { value } }) => {
    setAddressCheckSumCorrect(toChecksumAddress(value));
    setAddress(value);
  }, []);

  useEffect(() => {
    setSelectedIdentity(identities[0] ? identities[0].tokenId : null);
  }, [identities]);

  return {
    address,
    addressCheckSumCorrect,
    defaultSelectableIdentity,
    identitySelectableOptions,
    transfer,
    handleAddressChange,
    handleIdentityChange
  };
}
