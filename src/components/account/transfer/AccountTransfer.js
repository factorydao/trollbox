import classnames from 'classnames';
import Select from 'components/form/select';
import PropTypes from 'prop-types';
import { memo } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import Row from 'react-bootstrap/Row';
import './AccountTransfer.scss';
import useAccountTransfer from './useAccountTransfer';

function AccountTransfer({ className, show, onClose, identities }) {
  const {
    address,
    addressCheckSumCorrect,
    defaultSelectableIdentity,
    identitySelectableOptions,
    transfer,
    handleAddressChange,
    handleIdentityChange
  } = useAccountTransfer({
    identities
  });

  return (
    <Modal
      centered
      className={classnames('account-transfer', className)}
      show={show}
      onHide={onClose}
    >
      <Modal.Header>
        <Button variant="close" onClick={() => onClose()}></Button>
      </Modal.Header>
      <Modal.Body className="text-start">
        <h2 className="text-center">Transfer identities</h2>
        <Row>
          <Col>
            <Select
              className="account__identity-control"
              options={identitySelectableOptions}
              value={defaultSelectableIdentity}
              onChange={handleIdentityChange}
            />
            <br />
            <Form.Control
              placeholder="Address"
              type="text"
              value={address}
              onChange={handleAddressChange}
            />
            {address && !addressCheckSumCorrect && <div>Incorrect address checksum</div>}
          </Col>
        </Row>
      </Modal.Body>
      <Modal.Footer className="d-flex justify-content-center">
        <Button disabled={!addressCheckSumCorrect} variant="danger" onClick={() => transfer()}>
          Transfer
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

AccountTransfer.propTypes = {
  className: PropTypes.string,
  identities: PropTypes.object,
  show: PropTypes.bool,
  onClose: PropTypes.func
};

export default memo(AccountTransfer);
