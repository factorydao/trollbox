import { useMemo } from 'react';

export default function useAssetList({ items } = {}) {
  const hasItems = useMemo(() => items?.length > 0, [items]);

  return {
    hasItems
  };
}
