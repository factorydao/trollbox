import snapshot from 'tests/snapshot';
import AssetListItem from './AssetListItem';

describe('AssetListItem component', () => {
  it('should match snapshot', () => {
    snapshot(<AssetListItem icon="mock.png" name="My Name" symbol="mock" tickerSymbol="mock" />);
  });
});
