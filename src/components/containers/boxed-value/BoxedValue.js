import classnames from 'classnames';
import PropTypes from 'prop-types';
import { memo } from 'react';
import './BoxedValue.scss';

function BoxedValue({ className, label, value, ...props }) {
  return (
    <fieldset
      {...props}
      className={classnames('boxed-value', { 'boxed-value--no-value': !value }, className)}
    >
      <legend>{label}</legend>
      <div className="boxed-value__value">{value}</div>
    </fieldset>
  );
}

BoxedValue.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool
  ])
};

export default memo(BoxedValue);
