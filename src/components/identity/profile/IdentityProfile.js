import classnames from 'classnames';
import PropTypes from 'prop-types';
import { memo } from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { formatNumber } from 'utils/formats';
import ArtIdentityImage from '../form/ArtIdentityImage';
import './IdentityProfile.scss';
import useIdentityProfile from './useIdentityProfile';

function IdentityProfile({
  className,
  tokenId,
  // unclaimedV,
  unclaimedFVT
  // unclaimedRounds,
  // ...props
}) {
  const { chainId, currentV, goToIdentity } = useIdentityProfile({
    tokenId
  });

  return (
    <Card className={classnames('identity-profile-card', className)}>
      <div className="identity-profile">
        <Row className="align-items-end">
          <Col>
            {chainId.data && <ArtIdentityImage chainId={chainId.data} identityNum={tokenId} />}
          </Col>
          <Col className="identity-profile__title">
            <div>Identity /</div>
            <div className="identity-profile__title-id">{tokenId}</div>
          </Col>
        </Row>
        <Row className="identity-profile__markets-title">
          <Col>Markets</Col>
        </Row>
        <Row className="identity-profile__power align-items-center">
          <Col className="identity-profile__power-title">Macro</Col>
          <Col className="text-end">
            <span>$V Power </span>
            <strong>{currentV}</strong>
          </Col>
        </Row>
        <Row className="identity-profile__rewards align-items-center">
          <Col>
            <div className="title">Rewards</div>
            <Row>
              <Col>$FVT {formatNumber(unclaimedFVT || 0)}</Col>
            </Row>
          </Col>
        </Row>
        <Row>
          <Col className="text-end">
            <Button
              className="identity-profile__history-button"
              variant="primary"
              onClick={goToIdentity}
            >
              Voting history
            </Button>
          </Col>
        </Row>
      </div>
    </Card>
  );
}

IdentityProfile.propTypes = {
  className: PropTypes.string,
  tokenId: PropTypes.string.isRequired,
  fvtBalance: PropTypes.number,
  unclaimedFVT: PropTypes.number,
  unclaimedRounds: PropTypes.number,
  unclaimedTokens: PropTypes.number,
  unclaimedV: PropTypes.number
};

IdentityProfile.defaultProps = {
  fvtBalance: 0,
  unclaimedTokens: 0
};

export default memo(IdentityProfile);
