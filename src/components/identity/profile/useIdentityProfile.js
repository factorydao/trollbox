import { useChainId } from 'queries/token';
import { useCallback, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { getVoiceCredits } from 'utils/simpleGetters';

export default function useIdentityProfile({ tokenId } = {}) {
  const [tournamentId] = useState(process.env.REACT_APP_TOURNAMENT_ID);

  const [currentV, setCurrentV] = useState(0);

  const { data: chainId } = useChainId();

  const navigate = useNavigate();

  const goToIdentity = useCallback(() => {
    navigate(`./identity/${tokenId}`);
  }, [navigate, tokenId]);

  useEffect(() => {
    const updateV = async () => {
      const v = await getVoiceCredits(tournamentId, tokenId);

      setCurrentV(v);
    };

    updateV();
  }, [tokenId, tournamentId]);

  return {
    chainId,
    currentV,
    goToIdentity
  };
}
