import classnames from 'classnames';
import PropTypes from 'prop-types';
import { memo } from 'react';
import { Button, Col, Container, Row } from 'react-bootstrap';
import { formatNumber } from 'utils/formats';
// import { withdrawWinnings } from "utils/simpleSetters";
import './IdentityMarketCard.scss';
// import useIdentityMarketCard from "./useIdentityMarketCard";

function IdentityMarketCard({
  className,
  name,
  rewards,
  showBallot,
  roundId,
  // voterId,
  ...props
}) {
  console.log({ rewards });

  // const { claim, unclaimedRewards } = useIdentityMarketCard({
  //   rewards,
  //   roundId,
  //   voterId,
  //   withdrawWinnings,
  // });

  return (
    <Container {...props} className={classnames('identity-market-card', className)}>
      <Row>
        <Col md={2} xs={12}>
          <div className="title">Round</div>
          {name}
        </Col>
        <Col md="auto" xs={12}>
          <div className="title">{'Token Rewards'}</div>
          <Row>
            <Col sm="7" title={rewards} xs="6">
              {'FVT ' + formatNumber(rewards)}
            </Col>
          </Row>
        </Col>
        <Col className="identity-market-card__buttons">
          <Button
            className="identity-market-card__button"
            variant="outline"
            onClick={() => showBallot(roundId)}
          >
            View
          </Button>
        </Col>
      </Row>
    </Container>
  );
}

IdentityMarketCard.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string.isRequired,
  rewards: PropTypes.number,
  roundId: PropTypes.number,
  showBallot: PropTypes.func,
  voterId: PropTypes.string
};

IdentityMarketCard.defaultProps = {
  rewards: {
    vBonus: 0,
    fvtBonus: 0
  }
};

export default memo(IdentityMarketCard);
