import { useMemo, useState, useCallback } from 'react';

export default function useIdentityMarketCard({
  rewards,
  roundId,
  voterId,
  withdrawWinnings
} = {}) {
  const unclaimedRewards = useMemo(() => parseInt(rewards) > 0, [rewards]);

  const [claimError, setClaimError] = useState(null);

  const claim = useCallback(
    () => withdrawWinnings(voterId, roundId, setClaimError),
    [roundId, voterId, withdrawWinnings]
  );

  return {
    claim,
    claimError,
    unclaimedRewards
  };
}
