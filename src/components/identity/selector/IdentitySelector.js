import { memo } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Select, Statistic, Button } from 'antd';
import Loader from 'components/containers/loader';
import useIdentitySelector from './useIdentitySelector';
import './IdentitySelector.scss';

const { Option } = Select;

function IdentitySelector({ className, callback, ...props }) {
  const { createIdentity, createdIdentity, identities, identityPrice } = useIdentitySelector();

  return (
    <div {...props} className={classnames('identity-selector', className)}>
      <Loader
        error={identityPrice.error || createdIdentity.error}
        isLoading={identityPrice.isLoading || createdIdentity.isLoading}
      >
        <Statistic precision={3} title="Identity Price (FVT)" value={identityPrice.data} />
        <Loader error={identities.error} isLoading={identities.isLoading}>
          <Select defaultValue="Select Identity" style={{ width: 120 }} onChange={callback}>
            {identities.data.map((x) => (
              <Option key={x.txHash} value={x.tokenId}>
                {x.tokenId}
              </Option>
            ))}
          </Select>
        </Loader>
        <Button type="primary" onClick={() => createIdentity(identityPrice.data)}>
          Create Identity
        </Button>
      </Loader>
    </div>
  );
}

IdentitySelector.propTypes = {
  className: PropTypes.string,
  callback: PropTypes.func
};

export default memo(IdentitySelector);
