import { useCallback, useEffect, useMemo, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { getIdentitiesForAccount } from 'utils/simpleGetters';
import { withdrawWinnings } from 'utils/simpleSetters';

export default function useIdentity({ tokenId } = {}) {
  const [identities, setIdentities] = useState(null);

  const [selectedIdentity, setSelectedIdentity] = useState(null);

  const [isLoading, setIsLoading] = useState(false);

  const [claimError, setClaimError] = useState(null);

  const navigate = useNavigate();

  const claimWinnings = () => {
    const tournamentId = process.env.REACT_APP_TOURNAMENT_ID; // TODO: make dynamic

    console.log('claimWinnings', tournamentId, tokenId);

    withdrawWinnings(tournamentId, tokenId, setClaimError);
  };

  const identitySelectableOptions = useMemo(
    () =>
      identities
        ? Object.keys(identities).map((identity) => ({
            value: identity,
            label: `FVT ${identity}`
          }))
        : [],
    [identities]
  );

  console.log({ identitySelectableOptions, selectedIdentity, tokenId });

  const defaultSelectableIdentity = useMemo(
    () =>
      selectedIdentity &&
      identitySelectableOptions?.find(({ value }) => value === selectedIdentity.tokenId.toString()),
    [identitySelectableOptions, selectedIdentity]
  );

  const getIdentities = useCallback(async () => {
    setIsLoading(true);

    const ids = await getIdentitiesForAccount();

    setIdentities(ids);

    // const keys = Object.keys(ids);

    const idToSet = ids[tokenId];

    if (idToSet) {
      setSelectedIdentity(idToSet);
    }

    setIsLoading(false);
  }, [tokenId]);

  const handleIdentityChange = useCallback(
    ({ value }) => {
      setSelectedIdentity(identities[value]);
      navigate(`/account/identity/${value}`);
    },
    [identities, navigate]
  );

  useEffect(() => {
    getIdentities();
  }, [getIdentities]);

  return {
    defaultSelectableIdentity,
    identitySelectableOptions,
    isLoading,
    claimWinnings,
    claimError,
    selectedIdentity,
    handleIdentityChange
  };
}
