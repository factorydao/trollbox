import classnames from 'classnames';
import Loader from 'components/containers/loader';
import Select from 'components/form/select';
import IdentityCard from 'components/identity/card';
import PropTypes from 'prop-types';
import { memo } from 'react';
import { Button, Col, Container, Row } from 'react-bootstrap';
import './Identity.scss';
import useIdentity from './useIdentity';

function Identity({ className, tokenId, ...props }) {
  const {
    defaultSelectableIdentity,
    identitySelectableOptions,
    isLoading,
    selectedIdentity,
    claimWinnings,
    // claimError,
    handleIdentityChange
  } = useIdentity({
    tokenId
  });

  console.log({ selectedIdentity });

  return (
    <div {...props} className={classnames('identity', className)}>
      <Loader isLoading={isLoading}>
        <Container fluid>
          <Row className="identity__menu">
            <Col sm="8" xs="12">
              <Row className="identity__title">
                <div>My Account /</div>
                <Col>
                  <Select
                    className="identity__identity-control"
                    options={identitySelectableOptions}
                    value={defaultSelectableIdentity}
                    onChange={handleIdentityChange}
                  />
                </Col>
              </Row>
              <Col className="account__claim-power">
                <Button variant="primary" onClick={() => claimWinnings()}>
                  Claim All Winnings
                </Button>
              </Col>
              <Col className="identity__title">
                <div>Unclaimed Rewards {selectedIdentity?.unclaimedTokens}</div>
              </Col>
              {/* claimError &&
                <div>{claimError}</div> // TODO: what's the correct way to do this?
              */}
            </Col>
          </Row>
          <Row>
            <Col xs="12">
              {selectedIdentity ? (
                <IdentityCard
                  className="identity__identity-card"
                  fvtBalance={selectedIdentity.fvtBalance}
                  key={`identity-card-${selectedIdentity.tokenId}`}
                  tokenId={selectedIdentity.tokenId}
                  voteMarkets={selectedIdentity.voteMarkets}
                />
              ) : (
                <div>You do not have any identities associated with this account</div>
              )}
            </Col>
          </Row>
        </Container>
      </Loader>
    </div>
  );
}

Identity.propTypes = {
  className: PropTypes.string,
  tokenId: PropTypes.string
};

export default memo(Identity);
