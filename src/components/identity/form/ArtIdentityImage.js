import { memo } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import useArtIdentityImage from './useArtIdentityImage';

function ArtIdentityImage({ className, identityNum, chainId, ...props }) {
  const { url } = useArtIdentityImage({
    chainId,
    identityNum
  });

  return (
    <img
      {...props}
      alt=""
      className={classnames('identity-art', className)}
      src={url}
      width="100%"
    />
  );
}

ArtIdentityImage.propTypes = {
  className: PropTypes.string,
  chainId: PropTypes.number,
  identityNum: PropTypes.number
};

export default memo(ArtIdentityImage);
