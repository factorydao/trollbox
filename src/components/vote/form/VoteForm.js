import classnames from 'classnames';
import Loader from 'components/containers/loader';
import Spinner from 'components/indicators/Spinner';
import { useGlobalState } from 'globalState';
import html2canvas from 'html2canvas';
import PropTypes from 'prop-types';
import { memo, useRef } from 'react';
import { Button } from 'react-bootstrap';
import { BoxArrowUpRight } from 'react-bootstrap-icons';
import { formatHash } from 'utils/formats';
import { TRANSACTION_STATUS } from 'utils/helpers';
import List from './list';
import ProgressBar from './progress';
import CancelPrompt from './prompt';
import useVoteForm from './useVoteForm';
import './VoteForm.scss';

function VoteForm({
  className,
  items,
  selectedIdentity,
  tokensAvailable,
  tournamentId,
  updateRoundId,
  alreadyVoted,
  interfaceDisabled,
  ...props
}) {
  const {
    cancelPrompt,
    hasVoted,
    submitVote,
    submitVoteError,
    submitVoteIsLoading,
    submitVoteResult,
    syntheticItems,
    tokensLeft,
    tokensSpent,
    transactionStatus,
    handleCancel,
    handleExit,
    handleHideCancelPrompt,
    handleSubmit
  } = useVoteForm({
    items,
    selectedIdentity,
    tokensAvailable,
    tournamentId,
    updateRoundId,
    alreadyVoted,
    interfaceDisabled
  });
  const formRef = useRef();
  const [chainData] = useGlobalState('chainData');
  const downloadVoteImage = () => {
    html2canvas(formRef.current, { logging: false }).then((canvas) => {
      const dataURL = canvas.toDataURL('base64', 1);
      const elem = document.createElement('a');
      elem.href = dataURL;
      elem.download = 'voteresult.png';
      elem.click();
    });
  };
  //  console.log({ alreadyVoted, hasVoted, interfaceDisabled, tokensAvailable });

  return (
    <>
      <div {...props} className={classnames('vote-form', className)}>
        <Loader error={submitVote.error} isLoading={submitVoteIsLoading} showContentOnError>
          {cancelPrompt && (
            <CancelPrompt show onClose={handleHideCancelPrompt} onExit={handleExit} />
          )}
          {/* TODO: are hasVoted and alreadyVoted the same thing? */}
          {hasVoted || alreadyVoted ? (
            <div className="vote-form__success-message">
              <strong>Your vote has been submitted</strong>
            </div>
          ) : interfaceDisabled ? (
            <div className="vote-form__success-message">
              <strong>Voting is closed, the market window is now in progress</strong>
            </div>
          ) : (
            <div className="vote-form__progress-bar">
              <div className="vote-form__progress-bar__label">
                {selectedIdentity ? (
                  <span>{tokensLeft} $V Left</span>
                ) : (
                  <span>Connect your Identity to start voting</span>
                )}
              </div>
              <ProgressBar max={tokensAvailable} now={tokensLeft} />
            </div>
          )}
          <List ref={formRef} items={syntheticItems} sort={hasVoted} />
          <div className="vote-form__actions">
            {!hasVoted && !alreadyVoted && !interfaceDisabled && (
              <Button
                className="vote-form__submit-button"
                disabled={tokensSpent <= 0 || tokensLeft < 0}
                variant="secondary"
                onClick={handleSubmit}
              >
                {submitVoteError ? 'Try again' : 'Vote'}
              </Button>
            )}
          </div>
        </Loader>
      </div>
      <div className="vote-form__footer">
        {submitVoteError && (
          <div className="text-danger">
            {submitVoteError?.error?.data?.message || submitVoteError?.message}
          </div>
        )}
        {tokensLeft < 0 && (
          <div className="text-danger vote-text-danger">
            Over allocated, please adjust your votes to get your $FVT left as close to 0 as possible
          </div>
        )}
        {!alreadyVoted && !hasVoted && tokensSpent > 0 && (
          <Button className="vote-form__cancel-button" variant="link" onClick={handleCancel}>
            Cancel vote
          </Button>
        )}
        {submitVoteResult?.hash && (
          <div>
            {transactionStatus === TRANSACTION_STATUS.FAILED ? (
              <div className="text-danger">
                Sorry, your vote has failed. Please try again or contact our support.
              </div>
            ) : null}

            <div className="text-info d-inline-flex">
              {transactionStatus === TRANSACTION_STATUS.WAITING ? (
                <>
                  <Spinner className="vote-form__footer__spinner" />
                  Transaction submitted, awaiting confirmation!
                </>
              ) : null}
            </div>

            <div className="text-break">
              Hash: {formatHash(submitVoteResult.hash)}
              <a
                href={`${chainData.explorer}/tx/${submitVoteResult.hash}`}
                rel="noreferrer"
                target="_blank"
              >
                <BoxArrowUpRight className="ms-2 mb-1" />
              </a>
            </div>
          </div>
        )}
      </div>
      {(hasVoted && transactionStatus !== TRANSACTION_STATUS.FAILED) ||
        (alreadyVoted && (
          <div className="mt-3 download">
            <Button
              className="vote-form__submit-button"
              variant="secondary"
              onClick={downloadVoteImage}
            >
              Download image
            </Button>
          </div>
        ))}
    </>
  );
}

VoteForm.propTypes = {
  className: PropTypes.string,
  items: PropTypes.array,
  selectedIdentity: PropTypes.object,
  tokensAvailable: PropTypes.number,
  tournamentId: PropTypes.number,
  updateRoundId: PropTypes.number
};

VoteForm.defaultProps = {
  tokensAvailable: 0
};

export default memo(VoteForm);
