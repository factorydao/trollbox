import classnames from 'classnames';
import List from 'components/asset-list';
import PropTypes from 'prop-types';
import { forwardRef, memo } from 'react';
import Item from './item';
import './VoteFormAssetList.scss';

const VoteFormAssetList = forwardRef(({ className, sort, ...props }, ref) => {
  return (
    <List
      {...props}
      ref={ref}
      className={classnames('vote-form-asset-list', className)}
      itemComponent={Item}
      sort={
        sort
          ? (item1, item2) => {
              const weight1 = item1.weight || 0;

              const weight2 = item2.weight || 0;

              if (weight1 < weight2) {
                return 1;
              }

              if (weight1 > weight2) {
                return -1;
              }

              return 0;
            }
          : () => 0
      }
    />
  );
});
VoteFormAssetList.displayName = 'VoteFormAssetList';
VoteFormAssetList.propTypes = {
  className: PropTypes.string,
  sort: PropTypes.bool
};

export default memo(VoteFormAssetList);
