import { useMemo } from 'react';

export default function useVoteFormAssetListItem({ maxVotes, tokensAvailable, ...options } = {}) {
  const cleanProps = useMemo(() => {
    const cleanProps = { ...options };

    delete cleanProps.decimals;
    delete cleanProps.priceFeed;
    delete cleanProps.showWeight;

    return cleanProps;
  }, [options]);

  const maxProgressValue = maxVotes || Math.floor(Math.sqrt(tokensAvailable)) || 0;

  return {
    cleanProps,
    maxProgressValue
  };
}
