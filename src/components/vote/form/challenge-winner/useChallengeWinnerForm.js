import moment from 'moment';
import numeral from 'numeral';
import { useCallback, useMemo, useState } from 'react';
import { challengeWinner } from 'utils/simpleSetters';

function formatDate(date) {
  return moment.unix(date).format('DD/MM/YY');
}

function formatBallotNumber(ballotNumber) {
  return numeral(ballotNumber).format('00');
}

export default function useChallengeWinnerForm({
  ballotNumber,
  resultsDate,
  startDate,
  votes
} = {}) {
  const [selectedWinner, setSelectedWinner] = useState(-1);

  const [termsAccepted, setTermsAccepted] = useState(false);

  const formattedBallotNumber = useMemo(() => formatBallotNumber(ballotNumber), [ballotNumber]);

  const formattedResultsDate = useMemo(() => formatDate(resultsDate), [resultsDate]);

  const formattedStartDate = useMemo(() => formatDate(startDate), [startDate]);

  const winnerSelectableOptions = useMemo(
    () =>
      votes?.map((vote, index) => ({
        value: index,
        label: vote.name,
        icon: vote.icon
      })) || [],
    [votes]
  );

  const challenge = useCallback(() => challengeWinner(selectedWinner), [selectedWinner]);

  const handleWinnerChange = useCallback(({ value }) => {
    setSelectedWinner(value);
  }, []);

  const handleAcceptTermsChange = useCallback(({ target: { checked } }) => {
    setTermsAccepted(checked);
  }, []);

  return {
    challenge,
    formattedBallotNumber,
    formattedResultsDate,
    formattedStartDate,
    selectedWinner,
    termsAccepted,
    winnerSelectableOptions,
    handleAcceptTermsChange,
    handleWinnerChange
  };
}
