import classnames from 'classnames';
import AssetCard, { AssetCardTypes } from 'components/asset-card';
import Select from 'components/form/select';
import PropTypes from 'prop-types';
import { memo } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import Row from 'react-bootstrap/Row';
import './ChallengeWinnerForm.scss';
import useChallengeWinnerForm from './useChallengeWinnerForm';

function ChallengeWinnerForm({
  className,
  show,
  ballotNumber,
  startDate,
  resultsDate,
  marketWinner,
  votes,
  onClose,
  ...props
}) {
  const {
    challenge,
    formattedBallotNumber,
    formattedResultsDate,
    formattedStartDate,
    selectedWinner,
    termsAccepted,
    winnerSelectableOptions,
    handleAcceptTermsChange,
    handleWinnerChange
  } = useChallengeWinnerForm({
    ballotNumber,
    resultsDate,
    startDate,
    votes
  });

  return (
    <Modal
      {...props}
      centered
      className={classnames('challenge-winner-form', className)}
      show={show}
      onHide={onClose}
    >
      <Modal.Body className="text-start">
        <h2 className="text-center">Challenge the Proposed Winner</h2>
        <Row>
          <Col xs="6">
            {formattedStartDate} - {formattedResultsDate}
            <div className="challenge-winner-form__ballot-num">Ballot #{formattedBallotNumber}</div>
          </Col>
          <Col>
            Current proposed winner
            <AssetCard
              asset={
                marketWinner && {
                  icon: marketWinner.icon,
                  name: marketWinner.name
                }
              }
              type={AssetCardTypes.PROPOSED}
            />
          </Col>
        </Row>
        <Row>
          <Col xs="6">
            <strong>Select your proposed winner</strong>
          </Col>
          <Col>
            <Select
              className="challenge-winner-form__winner-control"
              options={winnerSelectableOptions}
              onChange={handleWinnerChange}
            />
          </Col>
        </Row>
        <Row>
          <Col xs="6">
            <div>
              <strong>Stake $FVT</strong>
            </div>
            Connect your wallet to stake your FVT
          </Col>
          <Col>
            <div className="challenge-winner-form__stake-amount">50,000 $FVT</div>
          </Col>
        </Row>
        <Row>
          <Col>
            <Form.Check
              label="I understand that by clicking challenge I am depositing 50,000 $FVT. If you are incorrect the $FVT will be burned, no refunds."
              type="checkbox"
              onChange={handleAcceptTermsChange}
            />
          </Col>
        </Row>
      </Modal.Body>
      <Modal.Footer className="d-flex justify-content-center">
        <Button
          disabled={!termsAccepted || selectedWinner < 0}
          variant="danger"
          onClick={() => challenge()}
        >
          Challenge Winner
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

ChallengeWinnerForm.propTypes = {
  ballotNumber: PropTypes.number,
  className: PropTypes.string,
  marketWinner: PropTypes.shape({
    icon: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  }),
  resultsDate: PropTypes.instanceOf(Date),
  startDate: PropTypes.instanceOf(Date),
  show: PropTypes.bool,
  votes: PropTypes.array,
  onClose: PropTypes.func,
  onExit: PropTypes.func
};

export default memo(ChallengeWinnerForm);
