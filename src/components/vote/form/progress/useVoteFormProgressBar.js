import { useMemo } from 'react';

export default function useVoteFormProgressBar({ min, now } = {}) {
  const isNegative = useMemo(() => (now || 0) < (min || 0), [min, now]);

  return {
    isNegative
  };
}
