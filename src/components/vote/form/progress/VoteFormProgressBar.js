import classnames from 'classnames';
import PropTypes from 'prop-types';
import { memo } from 'react';
import ProgressBar from 'react-bootstrap/ProgressBar';
import useVoteFormProgressBar from './useVoteFormProgressBar';
import './VoteFormProgressBar.scss';

function VoteFormProgressBar({ className, min, now, ...props }) {
  const { isNegative } = useVoteFormProgressBar({
    min,
    now
  });

  return (
    <ProgressBar
      {...props}
      now={now}
      className={classnames(
        'vote-form-progress-bar',
        { 'vote-form-progress-bar--negative': isNegative },
        className
      )}
      variant={isNegative ? 'danger' : 'primary'}
    />
  );
}

VoteFormProgressBar.propTypes = {
  className: PropTypes.string,
  now: PropTypes.number,
  min: PropTypes.number
};

VoteFormProgressBar.defaultProps = {
  now: 0,
  min: 0
};

export default memo(VoteFormProgressBar);
