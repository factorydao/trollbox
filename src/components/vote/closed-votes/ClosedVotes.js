import classnames from 'classnames';
import Ballot from 'components/vote/ballot';
import ChallengeWinnerForm from 'components/vote/form/challenge-winner';
import PropTypes from 'prop-types';
import { memo } from 'react';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import './ClosedVotes.scss';
import useClosedVotes from './useClosedVotes';

function ClosedVotes({ className, ballots, ...props }) {
  const {
    ballotNumber,
    challengeWinner,
    hasBallots,
    marketWinner,
    resolveBallots,
    resultsDate,
    show,
    startDate,
    votes,
    handleClose
  } = useClosedVotes({
    ballots
  });

  return (
    <Container {...props} className={classnames('closed-votes', className)} fluid>
      <Row>
        <Col>
          <h1>Closed Votes</h1>
        </Col>
      </Row>
      <Row>
        {hasBallots ? (
          resolveBallots.map(
            (
              {
                number,
                startDate,
                endDate,
                resultsDate,
                projectedWinner,
                marketWinner,
                votes,
                voteSum,
                voteMax,
                proposal
              },
              index
            ) => (
              <Col key={`ballot${index}`} md={6} xs={12}>
                <Ballot
                  ballotNumber={number}
                  challengeWinner={challengeWinner}
                  className={classnames('closed-votes__ballot', `m${index % 2 ? 'l' : 'r'}-4`)}
                  endDate={endDate}
                  marketWinner={marketWinner}
                  projectedWinner={projectedWinner}
                  proposal={proposal}
                  resultsDate={resultsDate}
                  startDate={startDate}
                  voteMax={voteMax}
                  voteSum={voteSum}
                  votes={votes}
                />
              </Col>
            )
          )
        ) : (
          <Col>No available votes</Col>
        )}
      </Row>
      <Row>
        <ChallengeWinnerForm
          ballotNumber={ballotNumber}
          marketWinner={marketWinner}
          resultsDate={resultsDate}
          show={show}
          startDate={startDate}
          votes={votes}
          onClose={handleClose}
        />
      </Row>
    </Container>
  );
}

ClosedVotes.propTypes = {
  className: PropTypes.string,
  ballots: PropTypes.arrayOf(
    PropTypes.shape({
      number: PropTypes.number.isRequired,
      startDate: PropTypes.number.isRequired,
      endDate: PropTypes.number.isRequired,
      resultsDate: PropTypes.number.isRequired,
      projectedWinner: PropTypes.object.isRequired,
      marketWinner: PropTypes.object,
      votes: PropTypes.array.isRequired
    })
  )
};

export default memo(ClosedVotes);
