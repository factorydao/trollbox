import classnames from 'classnames';
import moment from 'moment';
import numeral from 'numeral';
import { useMemo } from 'react';

function formatDate(date) {
  return moment.unix(date).format('DD/MM/YY');
}

function formatBallotNumber(ballotNumber) {
  return numeral(ballotNumber).format('00');
}

export default function useVoteBallot({
  ballotNumber,
  marketWinner,
  resultsDate,
  startDate,
  voteMax,
  votes
} = {}) {
  const formattedBallotNumber = useMemo(() => formatBallotNumber(ballotNumber), [ballotNumber]);

  const fomrattedStartDate = useMemo(() => formatDate(startDate), [startDate]);

  const formattedResultsDate = useMemo(() => formatDate(resultsDate), [resultsDate]);

  const syntheticVotes = useMemo(
    () =>
      votes?.map((vote) => ({
        ...vote,
        maxVotes: voteMax,
        className: classnames(
          'vote-ballot__asset-list__item',
          {
            'vote-ballot__asset-list__item--highlight':
              marketWinner && marketWinner.name === vote.name
          },
          vote.className
        ),
        showWeight: true
      })),
    [marketWinner, voteMax, votes]
  );

  return {
    formattedBallotNumber,
    fomrattedStartDate,
    formattedResultsDate,
    syntheticVotes
  };
}
