import Image from './FinanceVoteLogo.svg';

function FinanceVoteLogo(props) {
  return <img {...props} alt="finacne.vote" src={Image} />;
}

export default FinanceVoteLogo;
