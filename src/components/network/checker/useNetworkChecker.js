import { useState, useEffect, useCallback } from 'react';
import { eventListeners, ethInstance } from 'evm-chain-scripts';

export default function useNetworkChecker({ supportedChains } = {}) {
  const [currentNetwork, setCurrentNetwork] = useState(null);

  const [show, setShow] = useState(false);

  const checkNetwork = useCallback(async () => {
    try {
      const chainId = await ethInstance.getChainId();

      const isOk = supportedChains.indexOf(chainId) > -1;

      if (isOk) {
        console.warn(`NetworkChecker: detected connection to ${chainId}`);
      } else {
        console.warn(
          `NetworkChecker: wrong network. Expected one of ${supportedChains}, detected ${chainId}`
        );
      }

      setShow(!isOk);
      setCurrentNetwork(chainId);
    } catch (err) {
      console.error(err);

      setShow(true);
    }
  }, [supportedChains]);

  useEffect(() => {
    checkNetwork();

    eventListeners.addEventListener(eventListeners.EVENTS.NETWORK_CHANGE, checkNetwork);

    return () => {
      eventListeners.removeEventListener(eventListeners.EVENTS.NETWORK_CHANGE, checkNetwork);
    };
  }, [checkNetwork]);

  return {
    currentNetwork,
    show,
    setShow
  };
}
