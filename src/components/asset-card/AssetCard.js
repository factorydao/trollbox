import { memo, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import moment from 'moment';
import useAssetCard from './useAssetCard';
import './AssetCard.scss';

export const AssetCardTypes = {
  PROJECTED: 'projected',
  PENDING: 'pending',
  COMPLETED: 'completed',
  PROPOSED: 'proposed'
};

function formatDate(date) {
  return moment.unix(date).format('DD/MM/YY');
}

function AssetCard({ className, type, title, asset, date, ...props }) {
  const { typedClassName } = useAssetCard({ type });

  return (
    <div {...props} className={classnames('asset-card', typedClassName, className)}>
      {title && <div className="asset-card__title">{title}</div>}
      <div className="asset-card__label">
        {asset ? (
          <Fragment>
            {asset.icon && <img alt="" className="asset-card__label__icon" src={asset.icon} />}
            {asset.name}
          </Fragment>
        ) : (
          <Fragment>TBA {formatDate(date)}</Fragment>
        )}
      </div>
    </div>
  );
}

AssetCard.propTypes = {
  className: PropTypes.string,
  type: PropTypes.oneOf(Object.values(AssetCardTypes)),
  title: PropTypes.string,
  asset: PropTypes.shape({
    icon: PropTypes.string,
    name: PropTypes.string.isRequired
  }),
  date: PropTypes.number
};

export default memo(AssetCard);
