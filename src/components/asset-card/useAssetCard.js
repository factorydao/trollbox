import { useMemo } from 'react';
import { AssetCardTypes } from './AssetCard';

export default function useAssetCard({ type } = {}) {
  const typedClassName = useMemo(() => {
    switch (type) {
      case AssetCardTypes.PROJECTED:
        return 'asset-card--projected';
      case AssetCardTypes.PENDING:
        return 'asset-card--pending';
      case AssetCardTypes.COMPLETED:
        return 'asset-card--completed';
      case AssetCardTypes.PROPOSED:
        return 'asset-card--proposed';
      default:
        return null;
    }
  }, [type]);

  return {
    typedClassName
  };
}
