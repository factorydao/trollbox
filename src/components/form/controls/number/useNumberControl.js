import numeral from 'numeral';
import { useCallback, useRef } from 'react';

const KEY_UP = 38;

const KEY_DOWN = 40;

const format = '-0[.][000000000000000000]';

export default function useNumberControl({ readOnly, value, onChange, index } = {}) {
  const controlRef = useRef();

  const formatValue = (value) => {
    if (!value || value <= 0) {
      return '';
    }

    const formattedValue = numeral(value).format(format);

    if (/\.$/.test(value)) {
      const parts = value.toString().match(/\.\d*$/);

      return `${formattedValue}${parts.length > 0 ? parts[0] : '.'}`;
    }

    return formattedValue;
  };

  const handleFocus = useCallback(() => {
    controlRef.current.select();
  }, []);

  const handleChange = ({ target: { value } }) => {
    const resValue = formatValue(value);
    onChange(index, resValue);
  };

  const handleKeyDown = ({ keyCode }) => {
    if (!readOnly) {
      switch (keyCode) {
        case KEY_UP:
          value++;
          break;
        case KEY_DOWN:
          value--;
          break;
        default:
          break;
      }

      const resValue = formatValue(value);
      onChange(index, resValue);
    }
  };

  return {
    controlRef,
    handleChange,
    handleFocus,
    handleKeyDown
  };
}
