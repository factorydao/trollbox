import { memo } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import ReactSelect from 'react-select';
import './Select.scss';

const rootClassName = 'select';

function Select({ className, ...props }) {
  return (
    <ReactSelect
      {...props}
      className={classnames(rootClassName, className)}
      classNamePrefix={rootClassName}
    />
  );
}

Select.propTypes = {
  className: PropTypes.string
};

export default memo(Select);
