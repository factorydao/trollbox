import PropTypes from 'prop-types';
import { createContext, useReducer } from 'react';
import Reducer from './Reducer';

const initialState = {
  identities: {}
};

export const Context = createContext(initialState);

const Store = ({ children }) => {
  const [state, dispatch] = useReducer(Reducer, initialState);
  console.log(state);
  return <Context.Provider value={[state, dispatch]}>{children}</Context.Provider>;
};

Store.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)])
};

export default Store;
