import snapshot from 'tests/snapshot';
import App from './App';

describe('App component', () => {
  it('should match snapshot', () => {
    snapshot(<App />);
  });
});
