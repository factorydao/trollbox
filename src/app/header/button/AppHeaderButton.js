import { memo } from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './AppHeaderButton.scss';

function AppHeaderButton({ className, to, ...props }) {
  return (
    <NavLink
      {...props}
      className={classnames('app-header-button', 'btn', 'btn-link', className)}
      to={to}
    />
  );
}

AppHeaderButton.propTypes = {
  className: PropTypes.string,
  to: PropTypes.string.isRequired
};

export default memo(AppHeaderButton);
