import './AppHeader.scss';

const HamburgerIcon = ({ className, onClick }) => {
  return (
    <button onClick={onClick} className={className}>
      <span className="line" />
      <span className="line" />
      <span className="line" />
    </button>
  );
};

export default HamburgerIcon;
