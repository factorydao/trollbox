import { ethInstance } from 'evm-chain-scripts';
import { useChainId } from 'queries/token';
import { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';

export default function useAppHeader() {
  const { data: chainId } = useChainId();
  const history = useLocation();
  const [show, setShow] = useState(!ethInstance.currentWalletProvider);

  const [showCustomSignIn, setShowCustomSignIn] = useState(false);

  const handleClose = () => setShow(false);

  const [isMobileMenuOpened, setIsMobileMenuOpened] = useState(false);

  const handleShow = () => setShow(true);

  const handleShowCustomSignIn = () => setShowCustomSignIn(true);

  const handleCloseCustomSignIn = () => setShowCustomSignIn(false);

  // const addChain = async (id, name, map) => {
  //   const { ethereum } = window

  //   if (id !== 1) {
  //     ethereum
  //       .request({
  //         method: 'wallet_addEthereumChain',
  //         params: [ {
  //           chainId: `0x${id.toString(16)}`,
  //           chainName: map.name,
  //           rpcUrls: [ map.rpcUrl ],
  //           nativeCurrency: {
  //             name: map.coinName,
  //             symbol: map.symbol, // 2-6 characters long
  //             decimals: 18
  //           },
  //           blockExplorerUrls: [ map.explorerUrl ]
  //         } ]
  //       })
  //       .then(() => {
  //         console.log('wallet_addEthereumChain success')
  //       })
  //       .catch((error) => {
  //         console.error('wallet_addEthereumChain error', error)
  //       })
  //   }
  // }
  useEffect(() => {
    setIsMobileMenuOpened(false);
  }, [history.pathname]);
  return {
    chainId,
    show,
    showCustomSignIn,
    handleClose,
    handleCloseCustomSignIn,
    handleShow,
    handleShowCustomSignIn,
    isMobileMenuOpened,
    setIsMobileMenuOpened
  };
}
