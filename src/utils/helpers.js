import memoize from 'lodash.memoize';
import NETWORKS from '../configs/networks.json';

export const TRANSACTION_STATUS = {
  NONE: -1,
  FAILED: 0,
  CONFIRMED: 1,
  WAITING: 2
};

export const getChainSettings = (chainId = 1) => {
  return NETWORKS[chainId];
};

export const getChainSettingsMemoize = memoize(getChainSettings);
