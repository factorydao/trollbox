import classnames from 'classnames';
import { ethInstance } from 'evm-chain-scripts';
import Page from 'pages/Page';
import PropTypes from 'prop-types';
import { Fragment, memo, useState } from 'react';
import Button from 'react-bootstrap/Button';
import { getBallotForRound, getCurrentRoundId, getTournament } from 'utils/simpleGetters';

function TestPage({ className }) {
  const [currentRound, setCurrentRound] = useState(null);

  const tournamentId = process.env.REACT_APP_TOURNAMENT_ID;

  const testCurrentRound = async () => {
    const v = await getCurrentRoundId(tournamentId);

    setCurrentRound(v);
    console.log('current round', v);
    alert(v);
  };

  const testGetTournament = async () => {
    const v = await getTournament(tournamentId);

    console.log('tournament', v);
    alert(Object.entries(v));
  };

  const testGetBallotForRound = async () => {
    const t = await getTournament(tournamentId);

    const chainId = await ethInstance.getChainId();

    console.log({ chainId });

    const v = await getBallotForRound(chainId, t, currentRound);

    console.log('ballot', v);
    alert(Object.entries(v));
  };

  return (
    <Page
      className={classnames('home-page', className)}
      howToPlay={false}
      leftContent={
        <Fragment>
          <Button variant="danger" onClick={() => testCurrentRound()}>
            Test current round Id
          </Button>
          <br />
          <br />
          <Button variant="danger" onClick={() => testGetTournament()}>
            Test get tournament
          </Button>
          <br />
          <br />
          <Button disabled={!currentRound} variant="danger" onClick={() => testGetBallotForRound()}>
            Test get ballot
          </Button>
        </Fragment>
      }
      title="Test BSC"
    />
  );
}

TestPage.propTypes = {
  className: PropTypes.string
};

export default memo(TestPage);
