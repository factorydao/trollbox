import { useEffect } from 'react';
import { useParams } from 'react-router-dom';

export default function useAccountIdentityPage() {
  const { tokenId } = useParams();

  useEffect(() => {
    console.log({ tokenId });
  }, [tokenId]);

  return {
    tokenId
  };
}
