import { Statistic } from 'antd';
import classnames from 'classnames';
import BoxedValue from 'components/containers/boxed-value';
import Loader from 'components/containers/loader';
import Select from 'components/form/select';
import Form from 'components/vote/form';
import { ConnectWallet } from 'evm-chain-scripts';
import Page from 'pages/Page';
import PropTypes from 'prop-types';
import { Fragment, memo } from 'react';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import { formatDateShort } from 'utils/formats';
import CustomProgressBar from './ProgressBar';
import './VotePage.scss';
import useVotePage from './useVotePage';

const { Countdown } = Statistic;

function VotePage({ className }) {
  const {
    defaultSelectableIdentity,
    endDate,
    handleIdentityChange,
    identitySelectableOptions,
    roundBonus,
    roundId,
    roundProgress,
    selectedIdentity,
    startDate,
    tokenList,
    tournament,
    tournamentId,
    updateRoundId,
    voteCost,
    vPerRound,
    tournamentSteps,
    isWalletConnected,
    alreadyVoted,
    dates: { voting, market },
    interfaceDisabled
  } = useVotePage();

  const currentStepData = tournamentSteps.find((step) => step.isCurrent) || {};
  const { name, countDownValue } = currentStepData;

  const showFundingInfo =
    Number(tournament?.rewardsOwed) + Number(tournament?.roundRewardTokens) >
    Number(tournament?.rewardFunding);

  return (
    <Page
      className={classnames('vote-page', className)}
      leftContent={
        <Container fluid>
          <Row>
            <Col>
              <h3>Round #{tournament?.currentRoundId}</h3>
              {isWalletConnected ? (
                <Fragment>
                  <div className="vote-page__caption">Select an identity to vote</div>
                  <Select
                    className="vote-page__identity-control"
                    options={identitySelectableOptions}
                    value={defaultSelectableIdentity}
                    onChange={handleIdentityChange}
                  />
                </Fragment>
              ) : (
                <ConnectWallet className="vote-page__connect-wallet-button" variant="link" />
              )}
              <div className="vote-page__token-balance">$V Power: {vPerRound}</div>
            </Col>
          </Row>
          <br />
          <Row>
            <Col>
              <div className="vote-page__caption">Tournament details</div>
              <div className="vote-page__progress">
                <Row>
                  <Col>
                    <div className="vote-page__countdown">
                      <div className="vote-page__close-state">{name} window closes in</div>
                      <Countdown title={''} value={countDownValue} onFinish={updateRoundId} />
                    </div>
                  </Col>
                </Row>
                <div className="vote-page__round-dates">
                  <Row>
                    <Col>{startDate && formatDateShort(startDate)}</Col>
                    <Col>
                      <div className="vote-page__end-date">
                        {endDate && formatDateShort(endDate)}
                      </div>
                    </Col>
                  </Row>
                </div>
                <div>
                  <CustomProgressBar steps={tournamentSteps} now={roundProgress} />
                </div>
              </div>
              <div className={classnames('vote-page__details', 'vote-page__details-multiple')}>
                <BoxedValue
                  label="Reward Pool"
                  value={`${roundBonus} ${tournament?.rewardTokenSymbol ?? ''}`}
                />
                <BoxedValue
                  label="Token Cost to Vote"
                  value={`${voteCost} ${tournament?.rewardTokenSymbol ?? ''}`}
                />
              </div>
              <div className={classnames('vote-page__details', 'vote-page__details-single')}>
                <BoxedValue label="Voting Window" value={`${voting}`} />
                <BoxedValue label="Market Window" value={`${market}`} />
              </div>
            </Col>
          </Row>
        </Container>
      }
      rightContent={
        <Loader error={roundId.error} isLoading={roundId.isLoading}>
          <>
            <Form
              className="vote-page__form"
              items={tokenList}
              selectedIdentity={selectedIdentity}
              tokensAvailable={vPerRound}
              tournamentId={tournamentId}
              alreadyVoted={alreadyVoted}
              interfaceDisabled={interfaceDisabled}
            />

            {showFundingInfo ? (
              <div className="vote-page__fund-info">Tournament needs funding</div>
            ) : null}
          </>
        </Loader>
      }
    />
  );
}

VotePage.propTypes = {
  className: PropTypes.string
};

export default memo(VotePage);
