import './VotePage.scss';
const colors = ['#3ab1ff', '#45e0cc', '#ff493e'];

const CustomProgressBar = ({ steps, now }) => {
  const progressColor = colors[steps.findIndex((step) => step.isCurrent)];

  return (
    <div
      className="custom-progress-bar"
      style={{
        '--width': `${now}%`,
        '--stepColor': progressColor
      }}
    >
      {steps.map((step, index) => {
        return (
          <div key={index} className="step" style={{ width: `${step.max - step.min}%` }}>
            <span>{step.name}</span>
          </div>
        );
      })}
    </div>
  );
};

export default CustomProgressBar;
