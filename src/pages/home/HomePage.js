import Countdown from 'antd/lib/statistic/Countdown';
import classnames from 'classnames';
import Card from 'components/containers/card';
import Spinner from 'components/indicators/Spinner';
import ClosedVotes from 'components/vote/closed-votes';
import Page from 'pages/Page';
import PropTypes from 'prop-types';
import { Fragment, memo } from 'react';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';
import './HomePage.scss';
import useHomePage from './useHomePage';

const SLOGANS = {
  voting: 'Vote Now to Predict Tomorrow’s Market',
  markets: 'Market Window Live, Next Vote Opens in',
  challenge: 'Market Window Settling, Challenge Period Live. Next Vote Opens in'
};

function HomePage({ className, ...props }) {
  const { marketState, timeLeft, ballots, previousBallotsAreLoading } = useHomePage();

  return (
    <Page
      {...props}
      bottomContent={
        <div>{previousBallotsAreLoading ? <Spinner /> : <ClosedVotes ballots={ballots} />}</div>
      }
      className={classnames('home-page', className)}
      howToPlay={false}
      leftContent={
        <Fragment>
          <p>
            Can you predict the future? Vote on a basket of tokens in daily tournaments. Got votes
            on the winning token? You earn a share of the reward pool. Prove your market knowledge
            and contribute to a collective intelligence.
          </p>
          <a className="home-page" href="https://mint.fvt.app/" rel="noreferrer" target="_blank">
            <Button variant="primary">Mint an sbt to vote</Button>
          </a>
        </Fragment>
      }
      rightContent={
        <Card className="home-page__identity-form">
          <p>
            <strong>
              {SLOGANS[marketState]} <Countdown value={timeLeft} title={''} />
            </strong>
          </p>
          <Link className="btn btn-secondary" to="/vote">
            Vote
          </Link>
          <p>To enter the tournament you must have a finance.vote SBT</p>
        </Card>
      }
      title="The cryptoeconomic prediction game. Make predictions, earn crypto"
    />
  );
}

HomePage.propTypes = {
  className: PropTypes.string
};

export default memo(HomePage);
