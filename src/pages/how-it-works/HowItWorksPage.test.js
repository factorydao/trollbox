import snapshot from 'tests/snapshot';
import { withRouter } from 'tests/wrappers/router';
import HowItWorksPage from './HowItWorksPage';

describe('HowItWorksPage component', () => {
  it('should match snapshot', () => {
    snapshot(withRouter(<HowItWorksPage />));
  });
});
