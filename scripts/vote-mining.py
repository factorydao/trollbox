import requests
import json
import csv

url = 'https://api.bscscan.com/api?module=account&action=txlist&address=0xA6Bb733a61f2f4F09090781CCCD80929c9234b3f&startblock=1&endblock=99999999&sort=asc&apikey=V2B211ZURU8417WZ4YVQEW4Q2P9FDGPBSZ'

j = requests.get(url).json()

votes = [tx for tx in j['result'] if '0x1afb9068' in tx['input']]

counts = {}

for vote in votes:
    try:
        counts[vote['from']] += 1
    except KeyError:
        counts[vote['from']] = 1

print('BSC {}'.format(len(counts)))

url = 'https://api.etherscan.io/api?module=account&action=txlist&address=0xEa6556E350cD0C61452a26aB34E69EBf6f1808BA&startblock=1&endblock=99999999&sort=asc&apikey=PSCTUWK4E9MPGDAEZTB87JQSQC44UYA3ZA'
j = requests.get(url).json()

votes = [tx for tx in j['result'] if '0x1afb9068' in tx['input']]

for vote in votes:
    try:
        counts[vote['from']] += 1
    except KeyError:
        counts[vote['from']] = 1


# url = 'https://ethtrader.github.io/donut.distribution/finance.vote.json'
# j = requests.get(url).json()
#
# for item in j:
#     addr = item['address']
#     if addr not in counts:
#         counts[addr] = 1

vote_factor = 1

amounts = {addr: counts[addr] * vote_factor for addr in counts}

# json.dump(amounts, open('./vote-mining.json', 'w'))
writer = csv.writer(open('./vote-mining.csv', 'w'))
writer.writerows(amounts.items())