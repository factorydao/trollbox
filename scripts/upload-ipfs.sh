#!/usr/bin/env bash

scp -r ./build/ ipfs-node-fv:~/launchpad/trollbox
ssh ipfs-node-fv "./add_ipfs.sh trollbox;exit;"
scp -r ./build/ dolphin:~/launchpad/trollbox
ssh dolphin "./add_ipfs.sh trollbox;exit;"
