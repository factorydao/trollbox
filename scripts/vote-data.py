import json
from web3 import Web3
import os
from dotenv import load_dotenv
import pandas as pd

load_dotenv(dotenv_path='.env', verbose=True)
INFURA_API_KEY = os.getenv("INFURA_API_KEY")

network = 'bsc'

if network == 'bsc':
    w3 = Web3(Web3.WebsocketProvider('wss://speedy-nodes-nyc.moralis.io/968d807b19666c1ba3d56b8d/bsc/mainnet/archive/ws'))
    trollboxAddress = '0xA6Bb733a61f2f4F09090781CCCD80929c9234b3f'
    referralAddress = '0x82d7630c5EB722557De6D76575C9a7b8DE718500'
    identityAddress = '0x951AED5E3554332BC2624D988c9c70d002D3Dba0'
    block_delta = 100000
else:
    w3 = Web3(Web3.WebsocketProvider('wss://mainnet.infura.io/ws/v3/{}'.format(INFURA_API_KEY)))
    trollboxAddress = '0xEa6556E350cD0C61452a26aB34E69EBf6f1808BA'
    earliestBlock = 11182262
    referralAddress = '0x798A709E12CcA28bFa7Ff4a6dAa044b5e0B5FA00'
    identityAddress = '0xf779cae120093807985d5F2e7DBB21d69be6b963'
    block_delta = 300000

# w3 = Web3(Web3.WebsocketProvider('wss://apis.ankr.com/wss/a50c4e5ca57f44fa93ca578cfa3618b0/b8bce2324667b39bccd282f57c97a27d/binance/full/main'))
print('Current working directory: {}'.format(os.getcwd()))
print('INFURA_API_KEY: {}'.format(INFURA_API_KEY))


def getVoteData():
    print('Getting vote data')
    latest_block = w3.eth.getBlock('latest')
    print('latest_block', latest_block.number)
    trollboxJson = json.load(open('./src/truffle/build/contracts/Trollbox.json', 'r'))
    trollboxContract = w3.eth.contract(address=trollboxAddress, abi=trollboxJson['abi'])
    to_block = latest_block.number
    events = []
    while True:
        if to_block < earliestBlock:
            break
        from_block = to_block - block_delta
        print('getting VoteOccurred events from block {} to block {}'.format(from_block, to_block))
        filter = trollboxContract.events.VoteOccurred.createFilter(fromBlock=from_block, toBlock=to_block)
        to_block = from_block
        events.extend(filter.get_all_entries())

    processedVoteEvents = [{**x, **x['args']} for x in events]

    df = pd.DataFrame(processedVoteEvents)
    df.drop(['args', 'metadata'], axis='columns', inplace=True)

    print('Computing voice credits used per vote')
    df['voiceCreditsUsed'] = df['weights'].apply(lambda x: sum([y ** 2 for y in x]))
    df['choicesMade'] = df['choices'].apply(lambda x: len(x))

    voiceCreditMap = {}
    tournamentId = 1

    def getVoiceCredits(voterId):
        if voterId in voiceCreditMap:
            return voiceCreditMap[voterId]
        else:
            print('Querying chain for voice credits for voter id {}'.format(voterId))
            vc = trollboxContract.functions.getVoiceCredits(tournamentId, voterId).call()
            voiceCreditMap[voterId] = vc
            return vc

    df['currentVoiceCredits'] = df['voterId'].apply(getVoiceCredits)
    df2 = pd.DataFrame([getTransactionGas(row) for _, row in df.iterrows()])
    vote_df = pd.concat([df, df2], axis='columns')

    identityJson = json.load(open('./src/truffle/build/contracts/Identity.json', 'r'))
    identityContract = w3.eth.contract(address=identityAddress, abi=identityJson['abi'])
    voterCount = {}
    for voterId in df['voterId'].unique():
        owner = identityContract.functions.ownerOf(int(voterId)).call()
        try:
            voterCount[owner] = voterCount[owner] + 1
        except KeyError:
            voterCount[owner] = 1

    voterCount2 = [{'voterIds': v, 'owner': k} for k, v in voterCount.items()]
    voter_df = pd.DataFrame(voterCount2)

    return vote_df, voter_df



def getReferralData():
    print('Getting referral data')
    referralJson = json.load(open('./src/truffle/build/contracts/ReferralProgram.json', 'r'))
    referralContract = w3.eth.contract(address=referralAddress, abi=referralJson['abi'])
    filter = referralContract.events.ReferralMade.createFilter(fromBlock="0x0")
    events = filter.get_all_entries()
    processedReferralEvents = [{**x, **x['args']} for x in events]

    df = pd.DataFrame(processedReferralEvents)
    map = {x['referee']: x['referer'] for _, x in df.iterrows()}
    return map


def getTransactionGas(row):
    print('getting transaction for {}'.format(row))
    num_retries = 5
    for _ in range(num_retries):
        try:
            rcpt = w3.eth.getTransactionReceipt(row['transactionHash'])
            tx = w3.eth.getTransaction(row['transactionHash'])
            weiSpent = rcpt['gasUsed'] * tx['gasPrice']
            ethSpent = w3.fromWei(weiSpent, 'ether')
            return {'gasUsed': rcpt['gasUsed'], 'gasPrice': tx['gasPrice'], 'ethSpent': ethSpent}
        except:
            pass
    raise Exception()


def getVotingPower():
    print('getting voting power')
    identityJson = json.load(open('./src/truffle/build/contracts/Identity.json', 'r'))
    identityContract = w3.eth.contract(address=identityAddress, abi=identityJson['abi'])
    trollboxJson = json.load(open('./src/truffle/build/contracts/Trollbox.json', 'r'))
    trollboxContract = w3.eth.contract(address=trollboxAddress, abi=trollboxJson['abi'])

    tournamentId = 1
    voiceCredits = []
    numIdentities = identityContract.functions.numIdentities().call()
    for voterId in range(1, numIdentities + 1):
        vc = trollboxContract.functions.getVoiceCredits(tournamentId, voterId).call()
        owner = identityContract.functions.ownerOf(voterId).call()
        voiceCredits.append({
            'voiceCredits': vc,
            'voterId': voterId,
            'owner': owner
        })

    return pd.DataFrame(voiceCredits)


def getOwnershipData():
    print('Getting vote data')
    trollboxJson = json.load(open('./src/truffle/build/contracts/Trollbox.json', 'r'))
    trollboxContract = w3.eth.contract(address=trollboxAddress, abi=trollboxJson['abi'])
    filter = trollboxContract.events.VoteOccurred.createFilter(fromBlock="0x0")

    events = filter.get_all_entries()
    processedVoteEvents = [{**x, **x['args']} for x in events]

    df = pd.DataFrame(processedVoteEvents)
    df.drop(['args', 'metadata'], axis='columns', inplace=True)

    identityJson = json.load(open('./src/truffle/build/contracts/Identity.json', 'r'))
    identityContract = w3.eth.contract(address='0xf779cae120093807985d5F2e7DBB21d69be6b963', abi=identityJson['abi'])
    voterCount = {}
    for voterId in df['voterId'].unique():
        owner = identityContract.functions.ownerOf(int(voterId)).call()
        try:
            voterCount[owner] = voterCount[owner] + 1
        except KeyError:
            voterCount[owner] = 1

    voterCount2 = [{'voterIds': v, 'owner': k} for k, v in voterCount.items()]
    return pd.DataFrame(voterCount2)


def getWinnerData():
    tournamentId = 1
    trollboxJson = json.load(open('./src/truffle/build/contracts/Trollbox.json', 'r'))
    trollboxContract = w3.eth.contract(address=trollboxAddress, abi=trollboxJson['abi'])
    numRounds = trollboxContract.functions.getCurrentRoundId(tournamentId).call()
    print(numRounds)
    d = []
    for roundId in range(1, numRounds-1):
        results = trollboxContract.functions.getRound(tournamentId, roundId).call()
        d.append({
            'roundId': roundId,
            'winnerIndex': results[1]
        })
    return pd.DataFrame(d)


def getTokensWon():
    print('getTokensWon')
    identityJson = json.load(open('./src/truffle/build/contracts/Identity.json', 'r'))
    identityContract = w3.eth.contract(address=identityAddress, abi=identityJson['abi'])
    trollboxJson = json.load(open('./src/truffle/build/contracts/Trollbox.json', 'r'))
    trollboxContract = w3.eth.contract(address=trollboxAddress, abi=trollboxJson['abi'])

    tokensWon = []
    numIdentities = identityContract.functions.numIdentities().call()
    for voterId in range(1, numIdentities + 1):
        tokensWon.append(trollboxContract.functions.tokensWon(voterId).call())
        print('Checking balance for token {}: {}'.format(voterId, tokensWon[-1]))

    return tokensWon


tokensWon = getTokensWon()
print('a')

voiceCredits = getVotingPower()
voiceCredits.to_csv('./voice-data-{}.csv'.format(network), index=False)

df = getWinnerData()
df.to_csv('./winner-data-{}.csv'.format(network), index=False)

voteDf, voterDf = getVoteData()
print('Linking referrals to votes')
referralMap = getReferralData()
voteDf['referer'] = voteDf.apply(lambda x: referralMap.get(x['voterId'], 0), axis='columns')

print('Writing to disk')
voteDf.to_csv('./vote-data-{}.csv'.format(network), index=False)
voterDf.to_csv('./voter-data-{}.csv'.format(network), index=False)

print('Done')