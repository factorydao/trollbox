import Page from "../pages/page";

const page = new Page();

let metamaskWalletAddress;

describe("Connect tests", () => {
  before(() => {
    cy.visit("/");
    page.acceptMetamaskAccessRequest();
    page.getMetamaskWalletAddress().then((address) => {
      metamaskWalletAddress = address;
    });
  });
  context("Connect metamask wallet", () => {
    it(`should login with success`, () => {
      expect(metamaskWalletAddress).to.equal(
        "0x662852A6dDC15f86f02DD48426a8A75BfF6FA20E"
      );
    });
  });
});
