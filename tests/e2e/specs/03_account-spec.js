import Account from "../pages/account/account-page";

const account = new Account();

describe("Account tests", () => {
  before(() => {
    account.visit();
  });
  context("Initial", () => {
    it(`claim button should be disabled`, () => {
      account.getProfileReward().each(($el) => {
        if (cy.wrap($el).contains("$V 0")) {
          cy.wrap($el).find(".claim").should("have.attr", "disabled");
        }
      });
    });
    it(`identity page should be opened`, () => {
      account.getHistoryButton().first().click();
      account.wait(15000);
      cy.get(".identity-card__title").should("contain", "DeFi Winners");
    });
  });
});
