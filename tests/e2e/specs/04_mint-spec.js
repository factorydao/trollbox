import Mint from "../pages/mint/mint-page";

const mint = new Mint();

describe("Mint tests", () => {
  before(() => {
    mint.visit();
  });
  context("Identity", () => {
    it(`should mint new identity successful`, () => {
      mint.getMintIdentityBtn().click();
      mint.wait(5000);
      mint.assignWindows();
    });
  });
});
