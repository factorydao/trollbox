/* eslint-disable ui-testing/no-hard-wait */
/* eslint-disable cypress/no-unnecessary-waiting */
import Page from "../page";

export default class MintPage extends Page {
  constructor() {
    super();
  }

  getMintIdentityBtn() {
    return cy.get(".mint-identity-button");
  }

  visit() {
    cy.visit("/#/identity");
    cy.wait(15000);
  }
}
